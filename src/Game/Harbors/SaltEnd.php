<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Ships\RoyalPatrolSchooner;

class SaltEnd extends AbstractHarbor
{
    public const NUMBER = 4;
    public const NAME = "Salt End";

    public function east(): ?Harbor
    {
        return IsleOfGrain::getInstance();
    }

    public function south(): ?Harbor
    {
        return PiratesHarbor::getInstance();
    }

    public function west(): ?Harbor
    {
        return Fishguard::getInstance();
    }

    public function ship(): Ship
    {
        return new RoyalPatrolSchooner();
    }
}