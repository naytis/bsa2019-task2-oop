<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Ships\RoyalPatrolSchooner;

class Fishguard extends AbstractHarbor
{
    public const NUMBER = 3;
    public const NAME = "Fishguard";

    public function north(): ?Harbor
    {
        return SaltEnd::getInstance();
    }

    public function east(): ?Harbor
    {
        return PiratesHarbor::getInstance();
    }

    public function south(): ?Harbor
    {
        return Southampton::getInstance();
    }

    public function ship(): Ship
    {
        return new RoyalPatrolSchooner();
    }
}