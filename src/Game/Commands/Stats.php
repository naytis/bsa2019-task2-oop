<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class Stats extends AbstractCommand
{
    private $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
        $this->name = "stats";
        $this->description = "shows stats of ship";
    }

    public function execute(Writer $writer, ?string $arg): void
    {
        $writer->writeln('Ship stats:');
        $writer->writeln($this->game->getPlayer()->stats());
    }
}