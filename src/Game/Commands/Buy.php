<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Exceptions\InvalidStatValue;
use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Ship;

class Buy extends AbstractCommand
{
    public const AVAILABLE_ARGS = ['strength', 'armour', 'luck', 'rum'];

    private $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
        $this->name = "buy <strength|armour|luck|rum>";
        $this->description = "buys skill or rum: 1 chest of gold - 1 item";
    }

    public function execute(Writer $writer, ?string $arg): void
    {
        if ($arg === null) {
            throw new \InvalidArgumentException ("Argument required.");
        }

        if (!(in_array($arg, self::AVAILABLE_ARGS))) {
            throw new \InvalidArgumentException ("Buying item '{$arg}' incorrect, choose from: strength, armour, luck, rum");
        }

        if (get_class($this->game->getHarbor()) !== $this->game::START_HARBOR) {
            throw new \Exception("You cant exchange gold in this harbor.");
        }

        $player = $this->game->getPlayer();

        if (!in_array('💰', $player->getHold())) {
            throw new \Exception("You dont have gold in the hold");
        }

        if ($arg === "rum") {

            $this->reduceGold($this->game->getPlayer());
            $player->setHold(array_merge($player->getHold(), ['🍾']));
            $rumQuantity = array_count_values($player->getHold())['🍾'];
            $writer->writeln("You\'ve bought a rum. Your hold contains {$rumQuantity} bottle(s) of rum.");

        } else {

            $getStat = "get" . ucfirst($arg);
            $setStat = "set" . ucfirst($arg);

            try {
                $player->$setStat($player->$getStat() + 1);
                $writer->writeln("You\'ve bought a {$arg}. Your {$arg} is {$player->$getStat()}.");
                $this->reduceGold($this->game->getPlayer());
            } catch (InvalidStatValue $e) {
                $writer->writeln("{$arg} has maximum value");
            }

        }
    }

    private function reduceGold(Ship $player): void
    {
        $hold = $player->getHold();
        unset($hold[array_search("💰", $player->getHold())]);
        $player->setHold(array_values($hold));
    }
}