<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class ExitGame extends AbstractCommand
{
    public function __construct()
    {
        $this->name = "exit";
        $this->description = "finishes the game";
    }

    public function execute(Writer $writer, ?string $arg): void
    {
        $writer->writeln("Bye!");
        die();
    }
}