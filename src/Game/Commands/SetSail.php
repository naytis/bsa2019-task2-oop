<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Exceptions\InvalidDirection;

class SetSail extends AbstractCommand
{
    public const AVAILABLE_ARGS = ['east', 'west', 'north', 'south'];
    private $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
        $this->name = "set-sail <east|west|north|south>";
        $this->description = "moves in given direction";
    }

    public function execute(Writer $writer, ?string $direction): void
    {
        if ($direction === null) {
            throw new \InvalidArgumentException ("Direction required.");
        }

        if (!(in_array($direction, self::AVAILABLE_ARGS))) {
            throw new \InvalidArgumentException ("Direction '{$direction}' incorrect, choose from: east, west, north, south");
        }

        $currentHarbor = $this->game->getHarbor();
        try {
            $this->game->setHarbor($currentHarbor->$direction());
            $writer->writeln($this->game->getHarbor()->info());
        } catch (InvalidDirection $exception) {
            $writer->writeln($exception->getMessage());
        }
    }

}