<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Ships\RoyalBattleShip;

class Grays extends AbstractHarbor
{
    public const NUMBER = 6;
    public const NAME = "Grays";

    public function south(): ?Harbor
    {
        return LondonDocks::getInstance();
    }

    public function west(): ?Harbor
    {
        return IsleOfGrain::getInstance();
    }

    public function ship(): Ship
    {
        return new RoyalBattleShip();
    }
}