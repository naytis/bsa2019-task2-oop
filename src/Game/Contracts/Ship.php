<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface Ship
{
    public const HOLD_CAPACITY = 3;

    public function setStrength(int $strength): void;
    public function getStrength(): int;

    public function setArmour(int $armour): void;
    public function getArmour(): int;

    public function setLuck(int $luck): void;
    public function getLuck(): int;

    public function setHealth(int $health): void;
    public function getHealth(): int;

    public function setHold(array $loot): void;
    public function getHold(): array;

    public function name(): string;
    public function stats(): string;
}