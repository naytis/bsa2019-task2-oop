<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Exceptions\InvalidStatValue;
use BinaryStudioAcademy\Game\Helpers\Stats;

abstract class AbstractShip implements Ship
{
    public const INVALID_VALUE = "Exceeding the maximum value of stat";

    protected $strength;
    protected $armour;
    protected $luck;
    protected $health;
    protected $hold;

    final public function setStrength(int $strength): void
    {
        if ($strength > Stats::MAX_STRENGTH) {
            throw new InvalidStatValue(self::INVALID_VALUE);
        }

        $this->strength = $strength;
    }

    final public function getStrength(): int
    {
        return $this->strength;
    }

    final public function setArmour(int $armour): void
    {
        if ($armour > Stats::MAX_ARMOUR) {
            throw new InvalidStatValue(self::INVALID_VALUE);
        }

        $this->armour = $armour;
    }

    final public function getArmour(): int
    {
        return $this->armour;
    }

    final public function setLuck(int $luck): void
    {
        if ($luck > Stats::MAX_LUCK) {
            throw new InvalidStatValue(self::INVALID_VALUE);
        }

        $this->luck = $luck;
    }

    final public function getLuck(): int
    {
        return $this->luck;
    }

    final public function setHealth(int $health): void
    {
        if ($health > Stats::MAX_HEALTH) {
            throw new InvalidStatValue(self::INVALID_VALUE);
        }

        $this->health = $health;
    }

    final public function getHealth(): int
    {
        return $this->health;
    }

    final public function setHold(array $loot): void
    {
        if (count($loot) > self::HOLD_CAPACITY) {
            throw new InvalidStatValue(self::INVALID_VALUE);
        }

        $this->hold = $loot;
    }

    final public function getHold(): array
    {
        return $this->hold;
    }

    public function name(): string
    {
        return static::NAME;
    }

    public function stats(): string
    {
        return "strength: {$this->getStrength()}" . PHP_EOL
                . "armour: {$this->getArmour()}" . PHP_EOL
                . "luck: {$this->getLuck()}" . PHP_EOL
                . "health: {$this->getHealth()}";
    }
}