<?php

require __DIR__ . '/vendor/autoload.php';

use BinaryStudioAcademy\Game;

$reader = new Game\Io\CliReader;
$writer = new Game\Io\CliWriter;
$random = new Game\Helpers\Random;

$game = new BinaryStudioAcademy\Game\Game($random);

$game->start($reader, $writer);
