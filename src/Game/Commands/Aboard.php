<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Exceptions\InvalidStatValue;
use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class Aboard extends AbstractCommand
{
    private $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
        $this->name = "aboard";
        $this->description = "collect loot from the ship";
    }

    public function execute(Writer $writer, ?string $arg): void
    {
        $game = $this->game;
        $player = $game->getPlayer();
        $enemy = $game->getEnemy();

        if (get_class($game->getHarbor()) === $game::START_HARBOR) {
            throw new \Exception("There is no ship to aboard");
        }

        if ($enemy->getHealth() !== 0) {
            throw new \Exception("You cannot board this ship, since it has not yet sunk");
        }

        try {
            $player->setHold(array_merge($player->getHold(), $enemy->getHold()));
            $writer->writeln("You got {$enemy->getHold()[0]}.");
            $enemy->setHold([]);
        } catch (InvalidStatValue $e) {
            $writer->writeln("Cant collect loot. Your hold is full.");
            if (in_array("🍾", $player->getHold())) {
                $writer->writeln("Try to drink 🍾 from your hold to free up the slot.");
            }
        }
    }
}