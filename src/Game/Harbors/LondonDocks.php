<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Ships\HMSRoyalSovereign;

class LondonDocks extends AbstractHarbor
{
    public const NUMBER = 8;
    public const NAME = "London Docks";

    public function north(): ?Harbor
    {
        return Grays::getInstance();
    }

    public function west(): ?Harbor
    {
        return Felixstowe::getInstance();
    }

    public function ship(): Ship
    {
        return new HMSRoyalSovereign();
    }
}