<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Exceptions\InvalidDirection;

abstract class AbstractHarbor implements Harbor
{
    public const INVALID_DIRECTION = "Harbor not found in this direction";

    public static function getInstance(): Harbor
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new static();
        }

        return $instance;
    }

    public function north(): ?Harbor
    {
        throw new InvalidDirection(self::INVALID_DIRECTION);
    }

    public function east(): ?Harbor
    {
        throw new InvalidDirection(self::INVALID_DIRECTION);
    }

    public function south(): ?Harbor
    {
        throw new InvalidDirection(self::INVALID_DIRECTION);
    }

    public function west(): ?Harbor
    {
        throw new InvalidDirection(self::INVALID_DIRECTION);
    }

    public function name(): string
    {
        return "Harbor " . static::NUMBER . ": " . static::NAME . ".";
    }

    public function info(): string
    {
        return $this->name() . PHP_EOL
            . "You see {$this->ship()->name()}: " . PHP_EOL
            . $this->ship()->stats();
    }
}