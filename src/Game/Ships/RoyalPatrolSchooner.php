<?php

namespace BinaryStudioAcademy\Game\Ships;

class RoyalPatrolSchooner extends AbstractShip
{
    public const NAME = "Royal Patrool Schooner";

    public function __construct()
    {
        $this->strength = 4;
        $this->armour = 4;
        $this->luck = 4;
        $this->health = 50;
        $this->hold = ['💰'];
    }
}