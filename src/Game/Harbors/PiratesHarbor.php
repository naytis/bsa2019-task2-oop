<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Ships\PirateShip;

class PiratesHarbor extends AbstractHarbor
{
    public const NUMBER = 1;
    public const NAME = "Pirates Harbor";

    public function north(): ?Harbor
    {
        return SaltEnd::getInstance();
    }

    public function south(): ?Harbor
    {
        return Southampton::getInstance();
    }

    public function west(): ?Harbor
    {
        return Fishguard::getInstance();
    }

    public function ship(): Ship
    {
        return new PirateShip();
    }

    public function info(): string
    {
        return $this->name() . PHP_EOL . "Your health is repared to 60.";
    }
}