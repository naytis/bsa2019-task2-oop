<?php

namespace BinaryStudioAcademy\Game\Ships;

class HMSRoyalSovereign extends AbstractShip
{
    public const NAME = "HMS Royal Sovereign";

    public function __construct()
    {
        $this->strength = 10;
        $this->armour = 10;
        $this->luck = 10;
        $this->health = 100;
        $this->hold = ['💰', '💰', '🍾'];
    }
}