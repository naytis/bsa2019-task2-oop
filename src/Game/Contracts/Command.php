<?php

namespace BinaryStudioAcademy\Game\Contracts;

use BinaryStudioAcademy\Game\Contracts\Io\Input;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

interface Command
{
    public function name(): string;
    public function description(): string;
    public function execute(Writer $writer, ?string $arg): void;
}