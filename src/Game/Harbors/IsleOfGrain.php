<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Ships\RoyalPatrolSchooner;

class IsleOfGrain extends AbstractHarbor
{
    public const NUMBER = 5;
    public const NAME = "Isle of Grain";

    public function east(): ?Harbor
    {
        return Grays::getInstance();
    }

    public function south(): ?Harbor
    {
        return Felixstowe::getInstance();
    }

    public function west(): ?Harbor
    {
        return SaltEnd::getInstance();
    }

    public function ship(): Ship
    {
        return new RoyalPatrolSchooner();
    }
}