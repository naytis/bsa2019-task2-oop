<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class WhereAmI extends AbstractCommand
{
    private $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
        $this->name = "whereami";
        $this->description = "shows current harbor";
    }

    public function execute(Writer $writer, ?string $arg): void
    {
        $writer->writeln($this->game->getHarbor()->name());
    }
}