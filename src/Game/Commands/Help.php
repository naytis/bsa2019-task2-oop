<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\ConsoleComposer;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class Help extends AbstractCommand
{
    private $consoleComposer;

    public function __construct(ConsoleComposer $consoleComposer)
    {
        $this->consoleComposer = $consoleComposer;
        $this->name = "help";
        $this->description = "shows this list of commands";
    }

    public function execute(Writer $writer, ?string $arg): void
    {
        $commands = $this->consoleComposer->getCommands();

        $writer->writeln('List of commands:');

        foreach ($commands as $command) {
            $writer->writeln("{$command->name()} - {$command->description()}");
        }
    }
}