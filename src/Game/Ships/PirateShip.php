<?php

namespace BinaryStudioAcademy\Game\Ships;

class PirateShip extends AbstractShip
{
    public const NAME = "Pirate Ship";

    public function __construct()
    {
        $this->strength = 4;
        $this->armour = 4;
        $this->luck = 4;
        $this->health = 60;
        $this->hold = [];
    }

    public function stats(): string
    {
        return parent::stats() . PHP_EOL . "hold: {$this->holdToString()}";
    }

    private function holdToString(): string
    {
        if (empty($this->getHold())) {
            return "[ _ _ _ ]";
        }

        $result = "[ ";
        for ($i = 0; $i < 3; $i++) {
            $result .= ($this->getHold()[$i] ?: "_") . " ";
        }
        $result .= "]";

        return $result;
    }
}