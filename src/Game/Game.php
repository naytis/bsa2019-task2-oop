<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Commands\Aboard;
use BinaryStudioAcademy\Game\Commands\Buy;
use BinaryStudioAcademy\Game\Commands\Drink;
use BinaryStudioAcademy\Game\Commands\ExitGame;
use BinaryStudioAcademy\Game\Commands\Fire;
use BinaryStudioAcademy\Game\Commands\Help;
use BinaryStudioAcademy\Game\Commands\SetSail;
use BinaryStudioAcademy\Game\Commands\Stats;
use BinaryStudioAcademy\Game\Commands\WhereAmI;
use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Harbors\PiratesHarbor;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Ships\HMSRoyalSovereign;

class Game
{
    public const START_HARBOR = PiratesHarbor::class;
    public const FINAL_BOSS = HMSRoyalSovereign::class;
    public const DEFAULT_PLAYER_HEALTH = 60;

    private $random;
    private $consoleComposer;
    private $startHarbor;
    private $currentHarbor;
    private $player;
    private $enemy;

    public function __construct(Random $random)
    {
        $this->random = $random;

        $harbor = call_user_func([self::START_HARBOR, "getInstance"]);

        $this->currentHarbor = $this->startHarbor = $harbor;
        $this->player = $harbor->ship();

        $this->consoleComposer = new ConsoleComposer();

        $this->consoleComposer->addCommand(new Help($this->consoleComposer));
        $this->consoleComposer->addCommand(new Stats($this));
        $this->consoleComposer->addCommand(new SetSail($this));
        $this->consoleComposer->addCommand(new Fire($this, new Math(), $random));
        $this->consoleComposer->addCommand(new Aboard($this));
        $this->consoleComposer->addCommand(new Buy($this));
        $this->consoleComposer->addCommand(new Drink($this));
        $this->consoleComposer->addCommand(new WhereAmI($this));
        $this->consoleComposer->addCommand(new ExitGame());
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln("Welcome to 'Battle Ship' game!");
        $writer->writeln("Write 'help' to get list of available commands.");

        do {
            $writer->write("\n> ");
            $input = trim($reader->read());
            try {
                $command = $this->consoleComposer->findCommand($input);
                $command->execute($writer, $this->consoleComposer->getArgument());
            } catch (\Exception $e) {
                $writer->writeln($e->getMessage());
            }
        } while(true);
    }

    public function run(Reader $reader, Writer $writer)
    {
        $writer->writeln('This method runs program step by step.');
        $input = trim($reader->read());
        try {
            $command = $this->consoleComposer->findCommand($input);
            $command->execute($writer, $this->consoleComposer->getArgument());
        } catch (\Exception $e) {
            $writer->writeln($e->getMessage());
        }
    }

    public function getEnemy(): ?Ship
    {
        return $this->enemy;
    }

    public function getPlayer(): Ship
    {
        return $this->player;
    }

    public function setHarbor(Harbor $harbor): void
    {
        $this->currentHarbor = $harbor;

        if (!($harbor instanceof $this->startHarbor)) {
            $this->enemy = $harbor->ship();
        } else {
            if ($this->player->getHealth() < self::DEFAULT_PLAYER_HEALTH) {
                $this->player->setHealth(self::DEFAULT_PLAYER_HEALTH);
            }
        }
    }

    public function getHarbor(): Harbor
    {
        return $this->currentHarbor;
    }

    public function getStartHarbor(): Harbor
    {
        return $this->startHarbor;
    }

    public function restart(): void
    {
        $this->setHarbor($this->getStartHarbor());
        $this->player->setStrength($this->player->getStrength() - 1);
        $this->player->setArmour($this->player->getArmour() - 1);
        $this->player->setLuck($this->player->getLuck() - 1);
        $this->player->setHold([]);
    }
}
