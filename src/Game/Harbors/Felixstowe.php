<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Ships\RoyalBattleShip;

class Felixstowe extends AbstractHarbor
{
    public const NUMBER = 7;
    public const NAME = "Felixstowe";

    public function north(): ?Harbor
    {
        return IsleOfGrain::getInstance();
    }

    public function east(): ?Harbor
    {
        return LondonDocks::getInstance();
    }

    public function west(): ?Harbor
    {
        return Southampton::getInstance();
    }

    public function ship(): Ship
    {
        return new RoyalBattleShip();
    }
}