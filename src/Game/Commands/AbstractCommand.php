<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command;

abstract class AbstractCommand implements Command
{
    protected $name;
    protected $description;

    final public function name(): string
    {
        return $this->name;
    }

    final public function description(): string
    {
        return $this->description;
    }
}