<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Exceptions\InvalidStatValue;
use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Helpers\Stats;

class Drink extends AbstractCommand
{
    public const HEALTH_PER_BOTTLE = 30;

    private $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
        $this->name = "drink";
        $this->description = "your captain drinks 1 bottle of rum and fill 30 points of health";
    }

    public function execute(Writer $writer, ?string $arg): void
    {
        $player = $this->game->getPlayer();

        if (!in_array("🍾", $player->getHold())) {
            throw new \Exception("You dont have rum in the hold");
        }

        try {
            $player->setHealth($player->getHealth() + self::HEALTH_PER_BOTTLE);
        } catch (InvalidStatValue $e) {
            $player->setHealth(Stats::MAX_HEALTH);
        }

        $this->reduceRum($player);

        $writer->writeln("You\'ve drunk a rum and your health filled to {$player->getHealth()}");
    }

    private function reduceRum(Ship $player): void
    {
        $hold = $player->getHold();
        unset($hold[array_search("🍾", $player->getHold())]);
        $player->setHold(array_values($hold));
    }
}