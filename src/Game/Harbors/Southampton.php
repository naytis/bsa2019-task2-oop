<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Contracts\Harbor;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Ships\RoyalPatrolSchooner;

class Southampton extends AbstractHarbor
{
    public const NUMBER = 2;
    public const NAME = "Southhampton";

    public function north(): ?Harbor
    {
        return PiratesHarbor::getInstance();
    }

    public function east(): ?Harbor
    {
        return Felixstowe::getInstance();
    }

    public function west(): ?Harbor
    {
        return Fishguard::getInstance();
    }

    public function ship(): Ship
    {
        /*if ($this->shipCache[self::NAME] === null
        || get_class($this->shipCache[self::NAME]) !== RoyalPatrolSchooner::class) {
            return new RoyalPatrolSchooner();
        }
        return $this->shipCache;*/
        return new RoyalPatrolSchooner();
    }
}