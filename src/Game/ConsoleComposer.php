<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Command;

class ConsoleComposer
{
    private $commands;
    private $argument;

    public function addCommand(Command $command)
    {
        $input = explode(" ", $command->name());
        $pattern = $input[0] . ($input[1] ? ' ([a-z]*)' : '');
        $pattern = '/' . $pattern . '$/';

        $this->commands[$pattern] = $command;
    }

    public function getCommands(): array
    {
        return $this->commands;
    }

    public function findCommand(string $input): Command
    {
        foreach ($this->commands as $pattern => $command) {
            preg_match($pattern, $input, $matches);

            if ($matches[0] !== $input) continue;

            $this->argument = $arg = $matches[1] ?: null;
            return $command;
        }

        throw new \Exception("Command '{$input}' not found");
    }

    public function getArgument(): ?string
    {
        return $this->argument;
    }
}