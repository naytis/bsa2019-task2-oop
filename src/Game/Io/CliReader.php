<?php

namespace BinaryStudioAcademy\Game\Io;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;

class CliReader implements Reader
{
    private $stream;

    private $argument;

    private $raw;

    public function __construct()
    {
        $this->stream = STDIN;
    }

    public function read(): string
    {
        return fgets($this->stream);
    }

    public function getStream()
    {
        return $this->stream;
    }

    public function parse(string $input): void
    {
        $this->raw = $input;
        $this->argument = explode(' ', $input)[1];
    }

    public function argument(): string
    {
        return $this->argument;
    }

    public function raw(): string
    {
        return $this->raw;
    }
}
