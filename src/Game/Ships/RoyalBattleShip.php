<?php

namespace BinaryStudioAcademy\Game\Ships;

class RoyalBattleShip extends AbstractShip
{
    public const NAME = "Royal Battle Ship";

    public function __construct()
    {
        $this->strength = 8;
        $this->armour = 8;
        $this->luck = 7;
        $this->health = 80;
        $this->hold = ['🍾'];
    }
}