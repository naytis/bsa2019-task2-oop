<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface Harbor
{
    public static function getInstance(): Harbor;

    public function north(): ?Harbor;
    public function east(): ?Harbor;
    public function south(): ?Harbor;
    public function west(): ?Harbor;

    public function ship(): Ship;
    public function name(): string;
    public function info(): string;
}