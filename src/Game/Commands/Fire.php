<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Ship;
use BinaryStudioAcademy\Game\Contracts\Helpers\Math;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class Fire extends AbstractCommand
{
    private $game;
    private $math;
    private $random;

    public function __construct(Game $game, Math $math, Random $random)
    {
        $this->game = $game;
        $this->math = $math;
        $this->random = $random;
        $this->name = "fire";
        $this->description = "attacks enemy's ship";
    }

    public function execute(Writer $writer, ?string $arg): void
    {
        $player = $this->game->getPlayer();
        $enemy = $this->game->getEnemy();

        if ($enemy === null) {
            throw new \Exception("There is no ship to fight");
        }

        $damageDone = $this->getDamage($player, $enemy);
        $this->reduceHealth($enemy, $damageDone);

        if ($enemy->getHealth() === 0) {
            if (get_class($enemy) === $this->game::FINAL_BOSS) {
                $writer->writeln("🎉🎉🎉Congratulations🎉🎉🎉");
                $writer->writeln("💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾");
            } else {
                $writer->writeln("{$enemy->name()} on fire. Take it to the boarding.");
            }
            return;
        }

        $damageReceived = $this->getDamage($enemy, $player);
        $this->reduceHealth($player, $damageReceived);

        if ($player->getHealth() === 0) {
            $writer->writeln("Your ship has been sunk.");
            $writer->writeln("You restored in the Pirate Harbor.");
            $writer->writeln("You lost all your possessions and 1 of each stats.");
            $this->game->restart();
            return;
        }

        $writer->writeln("{$enemy->name()} has damaged on: {$damageDone} points.");
        $writer->writeln("health: {$enemy->getHealth()}");
        $writer->writeln("{$enemy->name()} damaged your ship on: {$damageReceived} points.");
        $writer->writeln("health: {$player->getHealth()}");
    }

    private function getDamage(Ship $attacker, Ship $defender): int
    {
        $damage = $this->math->damage($attacker->getStrength(), $defender->getArmour());
        if (!($this->math->luck($this->random, $attacker->getLuck()))) {
            return 0;
        }
        
        return $damage;
    }
    
    private function reduceHealth(Ship $ship, int $damage): void
    {
        if ($ship->getHealth() - $damage < 0) {
            $ship->setHealth(0);
        } else {
            $ship->setHealth($ship->getHealth() - $damage);
        }
    }
}